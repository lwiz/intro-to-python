# Defining a Function
def square(a):
    return a**2
print(square)
print(square(3))
print()

# Specifying Types
def square(a: int) -> int:
    return a**2
def nothing() -> None:
    pass
print(nothing())
print()

# Modules
import random
for _ in range(2):
    print(random.random())
from random import random
for _ in range(2):
    print(random())
print()

# Higher Level Functions
from typing import Callable
def repeat(f: Callable, xs: list) -> list:
    return [f(x) for x in xs]
print(repeat(square, [1, 2, 3]))
print()

# Lambdas
f = lambda x: x+5
print(f(3))
