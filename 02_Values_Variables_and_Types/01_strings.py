# String Operations
abc = 'Always Be Closing' + "!!"
print(abc)
print()

ha = "ha" * 3
print(ha)

# String Functions
ho = ha.replace("a", "o")
print(ho)
print()

print(len(abc), len(ha))

# F-Strings
na = ha.replace("h", "ow")
print(f"{na} {na.upper()}")
print()

# Multiline Strings
print("""aaa
wowza
zzz""")
