# Basic Class
class Address:
    country = "USA"

    def __init__(self, street, city, state, zip):
        self.street = street
        self.city = city
        self.state = state
        self.zip = zip

taos = Address("2612 Guadalupe St", "Austin", "TX", 78705)
print(f"{taos}")
print(f"{taos.street}, {taos.city}, {taos.state} {taos.zip}, {taos.country}")
print()

# Class With Methods
class Person:
    def __init__(self, name: str, age: int) -> "Person":
        self.name = name
        self.age = age

    def print(self):
        print(f"{self.name} is {self.age} years old")

    def birthday(self):
        self.age += 1
        print(f"Happy {self.age}th birthday {self.name}!")

laura = Person("Laura", 24)
laura.print()
laura.birthday()
laura.print()
print()

# Special Methods
class Sum:
    def __init__(self, a, b):
        self.a = a
        self.b = b
    
    def __str__(self):
        return f"{self.a+self.b}"

    def __add__(self, other):
        return Sum(self.a + other.a, self.b + other.b)

s1 = Sum(3, 4)
print(f"{s1}")
s2 = Sum("abc", "def")
print(f"{s2}")
s3 = Sum("123", "456")
print(f"{s2+s3}")
