# 1. Write a program that takes user input of a length in feet and inches, then 
#    converts that length to centimeters. Output all calculated numbers.

# 2. Print a student's diploma from UT in the following format:
# 
#                    THE UNIVERSITY OF TEXAS                 
#                           AT AUSTIN                        
#                        has conferred on                    
#                        <<student name>>                    
#                         the degree of                      
#             Bachelor of Science in Computer Science        
#                 <<optional honors designation>>            
#    with all the rights and privileges thereto appertaining.
# 
#    Where each line is centered in the output. The program should ask for the 
#    student's name and GPA, then convert the GPA to a floating point number 
#    and determine their honors designation based on the following table:
# 
#    GPA             Honors Designation
#    ===================================
#    3.75 to 4.00    Summa Cum Laude
#    3.50 to 3.74    Magna Cum Laude
#    3.25 to 3.49    Cum Laude
# 
#    If the student has no honors designation, then that line of output should 
#    be skipped.
# 
#    Bonus: print the student's name in bold and their honor designation in red
