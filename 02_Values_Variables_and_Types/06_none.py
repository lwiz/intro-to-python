a = None
def np(x):
    print(x)
np(5)
np("yo")
np(a)
print()

def dp(x):
    # The below is equivalent to `if not x:`
    if x is None:
        print("none")
    else:
        print(type(x))
dp(5)
dp(a)
