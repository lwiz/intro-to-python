# Dict Operations
d = {
    "name": "Laura",
    "favorite number": 4,
    100: True
}
print(d)
print("Hi", d["name"], "!")
print("name" in d, "Laura" in d)
print()

# Dict Functions
print(len(d))
d["age"] = 24
del d[100]
print(d)

# Dict Comprehensions
d = {l: ord(l) for l in "Bababooey"}
print(d)
