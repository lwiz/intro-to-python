# List Operations
l = ["howdy do", "howdy don't"]
print(l)
print(l + [3, 5])
print(l * 2)
l = [1, 2, 4, 8]
print(min(l), max(l))
print()

# Indexing and Access
l = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
print(l[0], l[3])
print(l[1:6])
print(l[:11:2])
print(4 in l, 5.5 in l)
print()

# List Functions
print(len(l))
l.append(4)
l.insert(0, 4)
print(l)
print(l.count(4))
l.reverse()
print(l.pop(), l.pop(1))
print(l)
l.remove(7)
print(l)
l.sort()
print(l)
r = range(-4, 4)
print(r, ",", *r)
print()

# List Comprehensions
l = [-x for x in r]
print(l)
r = range(20)
l = [x for x in r if x % 3 == 0]
print(l)
l = [x**2 for x in [1, 3, 5]]
print(l)
