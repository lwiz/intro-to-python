# integers
x = 5 * 2**3 + 6
print(f"The number is {x}!")
y = -x
print(f"{x} == {abs(y)}")
print()

# floats
import math
r = 2.0
area = math.pi * r**2
print(f"area == {area} == {area:.2f}")
print()

# booleans
b = True
print(not b)
if b:
    print("Yes!")
print()

# Conversions
m = x / area
print(m)
print(int(m))
print(int(b))
print(float(b))
print(int(False))
print()

# Notations
n1 = 0b00001100
print(n1)
n2 = 0xAF
print(n2, 0xA * 16 + 0xF)
n3 = 2e1
print(n3)
