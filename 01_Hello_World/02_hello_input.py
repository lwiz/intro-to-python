# What's in a name? that which we call a rose
# By any other name would smell as sweet
name = input("What is your name? ")
print("Hello", name, "!")
