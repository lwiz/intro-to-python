# Basic Exception Handling
try:
    x = 5
    y = x / 0
    print("dividing by zero B)")
    print(f"{x} / 0 == {y}")
except ZeroDivisionError as e:
    print(f"{e}")
    print(f"{x}")
    #print(f"{y}")
print()

# Raising Your Own Exceptions
def faileven(x: int) -> int:
    if x % 2 == 0:
        raise ValueError
    return x
try:
    x = faileven(3)
    y = faileven(4)
except ZeroDivisionError:
    print("divided by zero")
except:
    print("failed somehow")
print()

# Finally and Else Blocks
try:
    print(faileven(3))
except:
    print("failed somehow")
else:
    print("it didn't fail!")
finally:
    print("did it work? who knows")
print()
