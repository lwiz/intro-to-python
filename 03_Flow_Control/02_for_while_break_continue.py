# For Loops
for x in range(3):
    print(x)
print()

# While Loops
n = 5
while n > 0:
    print(n)
    n -= 2
print()

# Break Statements
l = ["hi", "test", "bababooey", "boy howdy", "sup"]
for s in l:
    print(f"checking {s}...")
    if len(s) > 4:
        print(f"    {s} is longer than 4")
        break
else:
    print("none longer than 4")
print()

# Continue Statements
for x in range(10):
    if x % 2 != 0:
        continue
    print(x)
