# Basic Branching
n = 3 if True else 4
a = "A" * n
b = "B" * n

#if a == b:
if a == b.replace("B", "A"):
    print("yes!")
elif len(a) == 3:
    print("3a")
elif len(a) == 2:
    print("2a")
else:
    print("nothing :(")
print()

# Boolean Logic
a = True
b = False
if a and a:
    print("both true")

if a and b:
    print("no :(")
else :
    print("one or less true")
if a or b:
    print("at least one")
if not b:
    print("the opposite is true")
print()

# Comparisons
if 3 < 5:
    print("its lesser")
if 2 >= 2:
    print("its greater than or equal to")
if "B" > "A":
    print("its greater")
